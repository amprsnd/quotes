require 'sinatra'

configure do
  mime_type :json, 'application/json'
end

get '/' do
  send_file './public/index.html'
end

# get json with quotes
get '/data' do
  content_type :json
  send_file './public/quotes.json'
end

# modify json with quotes
post '/data' do

  request.body.rewind
  data = JSON.parse request.body.read

  json = open './public/quotes.json', 'w' do |f|

      f << "{\"quotes\": #{data['quotes']}}"

  end

  json.close

end