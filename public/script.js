(function () {

    var data = {
        quotes: [],
        totalQuotes: 0,
        maxQuotes: 10
    };

    // Get quotes list from server
    axios.get('/data')
        .then(function (response) {
            data.quotes = response.data.quotes;
            data.totalQuotes = data.quotes.length;
            render();
        })
        .catch(function (error) {
            // TODO: render error
            console.log(error);
        });

    // Render UI function
    function render() {

        // components

        // reports
        // TODO: bad decision, fix it.
        var report = {
            report: true,
            visible: false,
            alertSuccess: false,
            alertDanger: false
        };
        Vue.component('reports', {
            props: ['max'],
            template: '#reports',
            data: function () {
                return report
            }
        });

        // quotes counter
        Vue.component('quotes-counter', {
            props: ['total', 'max'],
            template: '#quotes-counter'
        });

        // add new quote
        Vue.component('quote-form', {
            props: ['quotes', 'total', 'max'],
            data: function() {
                return {
                    newQuote: '',
                    totalQuotes: this.total
                }
            },
            template: '#quote-form',
            methods: {

                addQuote: function () {

                    if (this.quotes.length != this.max && this.newQuote.length != 0) {

                        this.quotes.push(this.newQuote);
                        app.totalQuotes = this.quotes.length;
                        this.newQuote = '';

                        updateQuotesList();

                        report.visible = true;
                        report.alertSuccess = true;

                        setTimeout(function () {
                            report.visible = false;
                            report.alertSuccess = false;
                        }, 1000)

                    } else {

                        report.visible = true;
                        report.alertDanger = true;

                        setTimeout(function () {
                            report.visible = false;
                            report.alertDanger = false;
                        }, 1000)

                    }

                }

            }
        });

        // quotes list
        Vue.component('quotes-list', {
           props: ['quotes'],
            template: '#quotes-list',
            methods: {

                removeQuote: function (i) {

                    var deleteQuote = confirm('delete this quote?');

                    if (deleteQuote) {
                        app.quotes.splice(i, 1);
                        app.totalQuotes = app.quotes.length;

                        updateQuotesList();

                        // TODO: report about it.

                    }

                }

            }
        });

        // app root
        var app = new Vue({
            el: '#app',
            data: data
        });

    }

    function updateQuotesList() {

        axios.post('/data', {
            quotes: JSON.stringify(data.quotes)
        });

    }

}).call(this);
